import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    private JPanel root;
    private JButton SushiButton;
    private JButton RamenButton;
    private JButton UdonButton;
    private JTextPane textPane1;
    private JButton SobaButton;
    private JButton TakoyakiButton;
    private JButton UnazyuButton;
    private JTextPane Totalprice;
    private JTextPane Orderedfoods;
    private JButton CheckOutButton;
    private JLabel Total;
    private JButton EatHereButton;
    private JButton ToGoButton;


    int Sushiprice = 1200;
    int Ramenprice = 1100;
    int Udonprice = 800;
    int Unazyuprice = 3000;
    int Takoyakiprice = 500;
    int Sobaprice = 900;
    int total = 0;

    public FoodGUI() {
        SushiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sushi");
            }
        });
        RamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
            }
        });

        UdonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon");
            }
        });

        UnazyuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Unazyu");
            }
        });
        TakoyakiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Takoyaki");
            }
        });
        SobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Soba");
            }
        });

    }

    void order(String food) {
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );

        int Place = JOptionPane.showConfirmDialog(
                null,
                "Would you like to eat here?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null, "Thank you! Order for " + food + " received");
            String currentItems = Orderedfoods.getText();
            Orderedfoods.setText(currentItems + food + "\n");
            if (Place == 0) {
                if (food == "Sushi") {
                    total += Sushiprice * 1.1;
                } else if (food == "Ramen") {
                    total += Ramenprice * 1.1;
                } else if (food == "Udon") {
                    total += Udonprice * 1.1;
                } else if (food == "Unazyu") {
                    total += Unazyuprice * 1.1;
                } else if (food == "Takoyaki") {
                    total += Takoyakiprice * 1.1;
                } else if (food == "Soba") {
                    total += Sobaprice * 1.1;
                }
                Totalprice.setText(total + "yen");
            }
            else if(Place == 1){
                if (food == "Sushi") {
                    total += Sushiprice * 1.08;
                } else if (food == "Ramen") {
                    total += Ramenprice * 1.08;
                } else if (food == "Udon") {
                    total += Udonprice * 1.08;
                } else if (food == "Unazyu") {
                    total += Unazyuprice * 1.08;
                } else if (food == "Takoyaki") {
                    total += Takoyakiprice * 1.08;
                } else if (food == "Soba") {
                    total += Sobaprice * 1.08;
                }
                Totalprice.setText(total + "yen");
            }
        }
        CheckOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int  check = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(check == 0){
                    JOptionPane.showMessageDialog (null , "Thank you. The total price is " + total +" yen.");
                    Orderedfoods.setText("");
                    total = 0;
                    Totalprice.setText(total + "yen");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
